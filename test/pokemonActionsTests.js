import { assert } from "chai";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";

import * as actions from "./../src/actions/pokemonActions";

const middlewares = [ thunk ];
const mockStore = configureStore(middlewares);

const stubPokemonApiSuccess = initial => ({
  fetch: () => Promise.resolve({ data: initial }),
  fetchByType: () => Promise.resolve({ data: initial }),
  fetchMove: () => Promise.resolve({ data: initial }),
});

const stubPokemonApiFail = () => ({
  fetch: () => Promise.reject(),
  fetchByType: () => Promise.reject(),
  fetchMove: () => Promise.reject(),
});

const textConstant = "pokemonReducerTest";

const initialState = {
  search: {
    value: '',
    searching: false,
    finded: [],
    done: false,
  },
  registered: [],
  searchByType: {
    searching: false,
    finded: [],
    done: false,
  },
  searchMove: {
    value: '',
    searching: false,
    done: false,
    finded: '',
    visible: false,
  }
};

const initialPokemon = {
  id: 1,
  name: "bulbasaur",
};

const initialByType = {
  pokemon: [
    { pokemon: { id: 1, name: "bulbasaur" } },
    { pokemon: { id: 2, name: "ivysaur" } },
    { pokemon: { id: 3, name: "venusaur" } },
  ]
}

const initialMove = {
  effect_entries: [
    {
      short_effect: textConstant
    }
  ]
}

describe('#pokemonActions', () => {
  it('searchPokemons fail', () => {
    const expectedActions = [
      actions.updateSearchPokemons(),
      actions.updateSearchFail(),
    ]
    const store = mockStore(initialState);

    return store.dispatch(actions.searchPokemons(stubPokemonApiFail(), textConstant))
      .then(() => {
        assert.deepEqual(
          store.getActions(),
          expectedActions
        );
      });
  });

  it('searchPokemons success', () => {
    const expectedActions = [
      actions.updateSearchPokemons(),
      actions.updateSearchResult(initialPokemon),
    ]
    const store = mockStore(initialState);

    return store.dispatch(actions.searchPokemons(stubPokemonApiSuccess(initialPokemon), textConstant))
      .then(() => {
        assert.deepEqual(
          store.getActions(),
          expectedActions
        );
      });
  });

  it('searchPokemonsByType fail', () => {
    const expectedActions = [
      actions.updateSearchPokemonsByType(),
      actions.updateSearchFailByType(),
    ]
    const store = mockStore(initialState);

    return store.dispatch(actions.searchPokemonsByType(stubPokemonApiFail(), textConstant))
      .then(() => {
        assert.deepEqual(
          store.getActions(),
          expectedActions
        );
      });
  });

  it('searchPokemonsByType success', () => {
    const expectedActions = [
      actions.updateSearchPokemonsByType(),
      actions.updateSearchResultByType(initialByType),
    ]
    const store = mockStore(initialState);

    return store.dispatch(actions.searchPokemonsByType(stubPokemonApiSuccess(initialByType), textConstant))
      .then(() => {
        assert.deepEqual(
          store.getActions(),
          expectedActions
        );
      });
  });

  it('searchPokemonMove fail', () => {
    const expectedActions = [
      actions.updateSearchPokemonsMove(textConstant),
      actions.updateSearchFailMove(),
    ]
    const store = mockStore(initialState);

    return store.dispatch(actions.searchPokemonMove(stubPokemonApiFail(), textConstant))
      .then(() => {
        assert.deepEqual(
          store.getActions(),
          expectedActions
        );
      });
  });

  it('searchPokemonMove success', () => {
    const expectedActions = [
      actions.updateSearchPokemonsMove(textConstant),
      actions.updateSearchResultMove(initialMove),
    ]
    const store = mockStore(initialState);

    return store.dispatch(actions.searchPokemonMove(stubPokemonApiSuccess(initialMove), textConstant))
      .then(() => {
        assert.deepEqual(
          store.getActions(),
          expectedActions
        );
      });
  });
});
