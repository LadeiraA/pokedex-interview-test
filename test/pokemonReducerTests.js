import { assert } from "chai";

import * as actions from "./../src/actions/pokemonActions";
import reducer from "./../src/reducers/pokemonReducer";

const textConstant = "pokemonReducerTest";

const initialState = {
    search: {
        value: '',
        searching: false,
        finded: [],
        done: false,
    },
    registered: [],
    searchByType: {
        searching: false,
        finded: [],
        done: false,
    },
    searchMove: {
        value: '',
        searching: false,
        done: false,
        finded: '',
        visible: false,
    }
};

const initialPokemon = {
    id: 1,
    name: "bulbasaur",
};

const initialPokemon2 = {
    id: 2,
    name: "ivysaur",
};

const initialPokemon3 = {
    id: 3,
    name: "venusaur",
};

const initialByType = {
    pokemon: [
        { pokemon: { id: 1, name: "bulbasaur" } },
        { pokemon: { id: 2, name: "ivysaur" } },
        { pokemon: { id: 3, name: "venusaur" } },
    ]
}

const initialMove = {
    effect_entries: [
        {
            short_effect: textConstant
        }
    ]
}

describe('#pokemonReducer - individual test', () => {
    it('Must return initial state', () => {
        assert.deepEqual(
            reducer(undefined, {}),
            initialState
        );
    });

    //UPDATE_SEARCH_VALUE
    it('updateSearchValue -> Must return updated [search.value] value', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchValue(textConstant)),
            { ...initialState, search: { ...initialState.search, value: textConstant } }
        );
    });

    //UPDATE_SEARCH_POKEMONS
    it('updateSearchPokemons -> Must return updated [search.searching] value', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchPokemons()),
            { ...initialState, search: { ...initialState.search, searching: true } }
        );
    });

    //UPDATE_SEARCH_RESULT
    it('updateSearchResult -> Must return updated [search.finded] and [search.done] values', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchResult(initialPokemon)),
            { ...initialState, search: { ...initialState.search, done: true, finded: [ { ...initialPokemon, selected: true } ] } }
        );
    });

    //UPDATE_SEARCH_FAIL
    it('updateSearchFail -> Must return updated [search.done] value', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchFail()),
            { ...initialState, search: { ...initialState.search, done: true } }
        );
    });

    //UPDATE_SEARCH_POKEMONS_BY_TYPE
    it('updateSearchPokemonsByType -> Must return updated [searchByType.searching] value', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchPokemonsByType()),
            { ...initialState, searchByType: { ...initialState.searchByType, searching: true } }
        );
    });

    //UPDATE_SEARCH_RESULT_BY_TYPE
    it('updateSearchResultByType -> Must return updated [searchByType.finded] and [searchByType.done] values', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchResultByType(initialByType)),
            { ...initialState, searchByType: { ...initialState.searchByType, done: true, finded: initialByType.pokemon.map(p => p.pokemon) } }
        );
    });

    //UPDATE_SEARCH_FAIL_BY_TYPE
    it('updateSearchFailByType -> Must return updated [searchByType.done] value', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchFailByType()),
            { ...initialState, searchByType: { ...initialState.searchByType, done: true } }
        );
    });

    //UPDATE_SEARCH_POKEMONS_MOVE
    it('updateSearchPokemonsMove -> Must return updated [searchMove.searching], [searchMove.visible] and [searchMove.value] values', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchPokemonsMove(textConstant)),
            { ...initialState, searchMove: { ...initialState.searchMove, searching: true, visible: true, value: textConstant } }
        );
    });

    //UPDATE_SEARCH_RESULT_MOVE
    it('updateSearchResultMove -> Must return updated [searchMove.finded] and [searchMove.done] values', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchResultMove(initialMove)),
            { ...initialState, searchMove: { ...initialState.searchMove, done: true, finded: initialMove.effect_entries[0].short_effect } }
        );
    });

    //UPDATE_SEARCH_FAIL_MOVE
    it('updateSearchFailMove -> Must return updated [searchMove.done] value', () => {
        assert.deepEqual(
            reducer(undefined, actions.updateSearchFailMove()),
            { ...initialState, searchMove: { ...initialState.searchMove, done: true } }
        );
    });

    //SEARCH_POKEMONS_MOVE_CLOSE
    it('searchPokemonMoveClose -> Must return initial state', () => {
        assert.deepEqual(
            reducer(undefined, actions.searchPokemonMoveClose()),
            { ...initialState }
        );
    });

    //SELECT_POKEMON
    it('selectPokemon -> Must return initial state', () => {
        assert.deepEqual(
            reducer(undefined, actions.selectPokemon(1)),
            { ...initialState }
        );
    });

    //REGISTER_POKEMON
    it('registerPokemon -> Must return updated [registered] value', () => {
        assert.deepEqual(
            reducer(undefined, actions.registerPokemon(initialPokemon)),
            { ...initialState, registered: [ ...initialState.registered, initialPokemon ] }
        );
    });

    //UNREGISTER_POKEMON
    it('unregisterPokemon -> Must return initial state', () => {
        assert.deepEqual(
            reducer(undefined, actions.unregisterPokemon(1)),
            { ...initialState }
        );
    });

    //SELECT_NEXT_POKEMON
    it('selectNextPokemon -> Must return initial state', () => {
        assert.deepEqual(
            reducer(undefined, actions.selectNextPokemon(1)),
            { ...initialState }
        );
    });

    //SELECT_PREV_POKEMON
    it('selectPrevPokemon -> Must return initial state', () => {
        assert.deepEqual(
            reducer(undefined, actions.selectPrevPokemon(1)),
            { ...initialState }
        );
    });
});

describe('#pokemonReducer - flux test', () => {

    it('Search fail flux', () => {
        let state = { ...initialState };
        let expectedState;

        //UPDATE_SEARCH_VALUE
        expectedState = { ...state, search: { ...state.search, value: textConstant, finded: [], done: false } };
        state = reducer(state, actions.updateSearchValue(textConstant));
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_POKEMONS
        expectedState = { ...state, search: { ...state.search, searching: true, finded: [], done: false } };
        state = reducer(state, actions.updateSearchPokemons());
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_FAIL
        expectedState = { ...state, search: { ...state.search, searching: false, finded: [], done: true } };
        state = reducer(state, actions.updateSearchFail());
        assert.deepEqual(state, expectedState);
    });

    it('Search success flux', () => {
        let state = { ...initialState };
        let expectedState;

        //UPDATE_SEARCH_VALUE
        expectedState = { ...state, search: { ...state.search, value: textConstant, finded: [], done: false } };
        state = reducer(state, actions.updateSearchValue(textConstant));
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_POKEMONS
        expectedState = { ...state, search: { ...state.search, searching: true, finded: [], done: false } };
        state = reducer(state, actions.updateSearchPokemons());
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_RESULT
        expectedState = { ...state, search: { ...state.search, done: true, finded: [ { ...initialPokemon, selected: true } ], searching: false } };
        state = reducer(state, actions.updateSearchResult(initialPokemon));
        assert.deepEqual(state, expectedState);
    });

    it('Register/unregister flux', () => {
        let state = { ...initialState };
        let expectedState;

        //REGISTER_POKEMON
        expectedState = { ...state, registered: [ ...state.registered, initialPokemon ] };
        state = reducer(state, actions.registerPokemon(initialPokemon));
        assert.deepEqual(state, expectedState);

        //UNREGISTER_POKEMON
        expectedState = { ...state, registered: state.registered.filter(p => p.id !== initialPokemon.id) };
        state = reducer(state, actions.unregisterPokemon(initialPokemon.id));
        assert.deepEqual(state, expectedState);
    });

    it('Navigation flux', () => {
        let state = { ...initialState };
        let expectedState;

        //REGISTER_POKEMON
        expectedState = { ...state, registered: [ ...state.registered, initialPokemon ] };
        state = reducer(state, actions.registerPokemon(initialPokemon));
        assert.deepEqual(state, expectedState);

        //REGISTER_POKEMON
        expectedState = { ...state, registered: [ ...state.registered, initialPokemon2 ] };
        state = reducer(state, actions.registerPokemon(initialPokemon2));
        assert.deepEqual(state, expectedState);

        //REGISTER_POKEMON
        expectedState = { ...state, registered: [ ...state.registered, initialPokemon3 ] };
        state = reducer(state, actions.registerPokemon(initialPokemon3));
        assert.deepEqual(state, expectedState);

        //SELECT_POKEMON
        expectedState = { ...state };
        expectedState.registered[0].selected = true;
        expectedState.registered[1].selected = false;
        expectedState.registered[2].selected = false;
        state = reducer(state, actions.selectPokemon(initialPokemon.id));
        assert.deepEqual(state, expectedState);

        //SELECT_NEXT_POKEMON
        expectedState = { ...state }
        expectedState.registered[0].selected = false;
        expectedState.registered[1].selected = true;
        expectedState.registered[2].selected = false;
        state = reducer(state, actions.selectNextPokemon());
        assert.deepEqual(state, expectedState);

        //SELECT_NEXT_POKEMON
        expectedState = { ...state }
        expectedState.registered[0].selected = false;
        expectedState.registered[1].selected = false;
        expectedState.registered[2].selected = true;
        state = reducer(state, actions.selectNextPokemon());
        assert.deepEqual(state, expectedState);

        //SELECT_NEXT_POKEMON
        expectedState = { ...state }
        expectedState.registered[0].selected = false;
        expectedState.registered[1].selected = false;
        expectedState.registered[2].selected = true;
        state = reducer(state, actions.selectNextPokemon());
        assert.deepEqual(state, expectedState);

        //SELECT_PREV_POKEMON
        expectedState = { ...state }
        expectedState.registered[0].selected = false;
        expectedState.registered[1].selected = true;
        expectedState.registered[2].selected = false;
        state = reducer(state, actions.selectPrevPokemon());
        assert.deepEqual(state, expectedState);

        //SELECT_PREV_POKEMON
        expectedState = { ...state }
        expectedState.registered[0].selected = true;
        expectedState.registered[1].selected = false;
        expectedState.registered[2].selected = false;
        state = reducer(state, actions.selectPrevPokemon());
        assert.deepEqual(state, expectedState);

        //SELECT_PREV_POKEMON
        expectedState = { ...state }
        expectedState.registered[0].selected = true;
        expectedState.registered[1].selected = false;
        expectedState.registered[2].selected = false;
        state = reducer(state, actions.selectPrevPokemon());
        assert.deepEqual(state, expectedState);
    });

    it('Search by type fail flux', () => {
        let state = { ...initialState };
        let expectedState;

        //UPDATE_SEARCH_POKEMONS_BY_TYPE
        expectedState = { ...state, searchByType: { ...state.searchByType, searching: true, finded: [], done: false } };
        state = reducer(state, actions.updateSearchPokemonsByType());
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_FAIL_BY_TYPE
        expectedState = { ...state, searchByType: { ...state.searchByType, searching: false, finded: [], done: true } };
        state = reducer(state, actions.updateSearchFailByType());
        assert.deepEqual(state, expectedState);
    })

    it('Search by type success flux', () => {
        let state = { ...initialState };
        let expectedState;

        //UPDATE_SEARCH_POKEMONS_BY_TYPE
        expectedState = { ...state, searchByType: { ...state.searchByType, searching: true, finded: [], done: false } };
        state = reducer(state, actions.updateSearchPokemonsByType());
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_RESULT_BY_TYPE
        expectedState = { ...state, searchByType: { ...state.searchByType, done: true, finded: initialByType.pokemon.map(p => p.pokemon), searching: false } };
        state = reducer(state, actions.updateSearchResultByType(initialByType));
        assert.deepEqual(state, expectedState);
    })

    it('Search move fail flux', () => {
        let state = { ...initialState };
        let expectedState;

        //UPDATE_SEARCH_POKEMONS_MOVE
        expectedState = { ...state, searchMove: { ...state.searchMove, searching: true, visible: true, value: textConstant } };
        state = reducer(state, actions.updateSearchPokemonsMove(textConstant));
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_FAIL_MOVE
        expectedState = { ...state, searchMove: { ...state.searchMove, searching: false, finded: '', done: true } };
        state = reducer(state, actions.updateSearchFailMove());
        assert.deepEqual(state, expectedState);

        //SEARCH_POKEMONS_MOVE_CLOSE
        expectedState = { ...state, searchMove: { ...initialState.searchMove } };
        state = reducer(state, actions.searchPokemonMoveClose());
        assert.deepEqual(state, expectedState);
    });

    it('Search move success flux', () => {
        let state = { ...initialState };
        let expectedState;

        //UPDATE_SEARCH_POKEMONS_MOVE
        expectedState = { ...state, searchMove: { ...state.searchMove, searching: true, visible: true, done: false, finded: '', value: textConstant } };
        state = reducer(state, actions.updateSearchPokemonsMove(textConstant));
        assert.deepEqual(state, expectedState);

        //UPDATE_SEARCH_RESULT_MOVE
        expectedState = { ...state, searchMove: { ...state.searchMove, searching:false, done: true, finded: initialMove.effect_entries[0].short_effect } };
        state = reducer(state, actions.updateSearchResultMove(initialMove));
        assert.deepEqual(state, expectedState);

        //SEARCH_POKEMONS_MOVE_CLOSE
        expectedState = { ...state, searchMove: { ...initialState.searchMove } };
        state = reducer(state, actions.searchPokemonMoveClose());
        assert.deepEqual(state, expectedState);
    });

});
