import './assets/main.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route } from 'react-router'
import { ConnectedRouter } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

import store from './store';

import PokedexControls from './containers/pokedexControls';
import PokemonRegisteredList from './containers/pokemonRegisteredList';
import PokemonSearch from './containers/pokemonSearch';
import PokemonDetail from './containers/pokemonDetail';
import PokemonTypeList from './containers/pokemonTypeList';

const history = createHistory();

ReactDOM.render(
  <Provider store={store(history)}>
    <ConnectedRouter history={history}>
      <div id="app-container">
        <div id="screen">
          <Route exact path="/" component={PokemonRegisteredList}/>
          <Route path="/search" component={PokemonSearch}/>
          <Route path="/detail" component={PokemonDetail}/>
          <Route path="/type-list/:type" component={PokemonTypeList}/>
        </div>
        <Route path="/" component={PokedexControls}/>
      </div>
    </ConnectedRouter>
  </Provider>,
  document.querySelector('#container')
);
