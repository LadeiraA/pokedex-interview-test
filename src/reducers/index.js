import { combineReducers } from 'redux';

import { routerReducer } from 'react-router-redux';

import pokemonReducer from './pokemonReducer';

const reducers = combineReducers({
  pokemon: pokemonReducer,
  router: routerReducer,
});

export default reducers;
