import {
  UPDATE_SEARCH_VALUE,
  UPDATE_SEARCH_POKEMONS,
  UPDATE_SEARCH_RESULT,
  UPDATE_SEARCH_FAIL,
  UPDATE_SEARCH_POKEMONS_BY_TYPE,
  UPDATE_SEARCH_RESULT_BY_TYPE,
  UPDATE_SEARCH_FAIL_BY_TYPE,
  UPDATE_SEARCH_POKEMONS_MOVE,
  UPDATE_SEARCH_RESULT_MOVE,
  UPDATE_SEARCH_FAIL_MOVE,
  SEARCH_POKEMONS_MOVE_CLOSE,
  SELECT_POKEMON,
  REGISTER_POKEMON,
  UNREGISTER_POKEMON,
  SELECT_NEXT_POKEMON,
  SELECT_PREV_POKEMON,
} from './../actions/pokemonActions';

const initialState = {
  search: {
    value: '',
    searching: false,
    finded: [],
    done: false,
  },
  registered: [],
  searchByType: {
    searching: false,
    finded: [],
    done: false,
  },
  searchMove: {
    value: '',
    searching: false,
    done: false,
    finded: '',
    visible: false,
  }
};

export default (state = initialState, action) => {
  let search;
  let searchByType;
  let searchMove;
  let finded;
  let registered;
  let selectedId;

  const selectPokemon = (selectedId) => {
    finded = state.search.finded.map((pokemon) => {
      pokemon.selected = pokemon.id === selectedId;
      return pokemon;
    });
    search = { ...state.search, finded };
    registered = state.registered.map((pokemon) => {
      pokemon.selected = pokemon.id === selectedId;
      return pokemon;
    });

    return { ...state, registered, search, searchMove: { ...initialState.searchMove } };
  }

  switch (action.type) {
    case UPDATE_SEARCH_VALUE:
      search = { ...state.search, value: action.payload, finded: [], done: false };
      return { ...state, search };

    case UPDATE_SEARCH_POKEMONS:
      search = { ...state.search, searching: true, finded: [], done: false };
      return { ...state, search };

    case UPDATE_SEARCH_RESULT:
      finded = [ { ...action.payload } ];
      search = { ...state.search, searching: false, done: true, finded };
      state = { ...state, search };
      return selectPokemon(action.payload.id);

    case UPDATE_SEARCH_FAIL:
      search = { ...state.search, searching: false, finded: [], done: true };
      return { ...state, search };

    case UPDATE_SEARCH_POKEMONS_BY_TYPE:
      searchByType = { ...state.searchByType, searching: true, finded: [], done: false };
      return { ...state, searchByType };

    case UPDATE_SEARCH_RESULT_BY_TYPE:
      finded = action.payload.pokemon.map(p => p.pokemon);
      searchByType = { ...state.searchByType, searching: false, done: true, finded };
      return { ...state, searchByType };

    case UPDATE_SEARCH_FAIL_BY_TYPE:
      searchByType = { ...state.searchByType, searching: false, finded: [], done: true };
      return { ...state, searchByType };

    case UPDATE_SEARCH_POKEMONS_MOVE:
      searchMove = { ...state.searchMove, value: action.payload, visible: true, searching: true, done: false, finded: '' };
      return { ...state, searchMove };

    case UPDATE_SEARCH_RESULT_MOVE:
      finded = action.payload.effect_entries[0].short_effect;
      searchMove = { ...state.searchMove, searching: false, done: true, finded };
      return { ...state, searchMove };

    case UPDATE_SEARCH_FAIL_MOVE:
      searchMove = { ...state.searchMove, searching: false, finded: '', done: true };
      return { ...state, searchMove };

    case SEARCH_POKEMONS_MOVE_CLOSE:
      searchMove = { ...state.searchMove, value: '', visible: false, searching: false, done: false, finded: '' };
      return { ...state, searchMove };

    case SELECT_POKEMON:
      return selectPokemon(action.payload);

    case REGISTER_POKEMON:
      registered = [ ...state.registered, { ...action.payload } ];
      return { ...state, registered };

    case UNREGISTER_POKEMON:
      finded = state.registered.filter(p => p.id !== action.payload);
      registered = state.registered.filter(p => p.id !== action.payload);
      search = { ...state.search, finded };
      state = { ...state, registered, search };

      if (registered.length) {
        return selectPokemon(registered[0].id);
      }

      return state;

    case SELECT_NEXT_POKEMON:
      let next = 0;
      selectedId = 0;

      for(let i = 0; i < state.registered.length; i++) {
        if (state.registered[i].selected) {
          next = i + 1;
          break;
        }
      }

      if (state.registered.length > next) {
        selectedId = state.registered[next].id;
      }

      if (selectedId) {
        return selectPokemon(selectedId);
      }

      return state;

    case SELECT_PREV_POKEMON:
      let prev = -1;
      selectedId = 0;

      for(let i = 0; i < state.registered.length; i++) {
        if (state.registered[i].selected) {
          prev = i - 1;
          break;
        }
      }

      if (prev >= 0) {
        selectedId = state.registered[prev].id;
      }

      if (selectedId) {
        return selectPokemon(selectedId);
      }

      return state;

    default:
      return state;
  }
};
