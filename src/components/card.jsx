import React from 'react';
import './../assets/components/card.scss';

const Card = ({ children, onClick, className }) => {

  className = "card-section" + ((" " + className) || "");

  className += onClick ? " clickable" : "";

  onClick = onClick || (() => false);

  return (
    <div className={className} onClick={onClick}>
      {children}
    </div>
  );
}

export default Card;
