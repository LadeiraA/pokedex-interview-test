import './../assets/components/card.scss';

import React from 'react';

import Card from './../components/card';

const PokemonList = ({ pokemons, onClick, emptyMessage }) => {

  if (!pokemons.length){
    if (emptyMessage) {
      return <Card>{emptyMessage}</Card>
    }
    return null;
  }

  const pokemonList = [];

  let arrowUp;
  let arrowDown;
  let idxStart = 0;
  let idxEnd = 2;

  pokemons.map((pokemon, idx) => {
    if (pokemon.selected) {
      idxStart = idx - 1;
      idxEnd = idx + 1;
    }
  });

  if (idxStart < 0) {
    idxStart++;
    idxEnd = idxEnd <= pokemons.length - 1 ? idxEnd + 1 : pokemons.length - 1;
  }

  if (idxEnd > pokemons.length - 1) {
    idxStart = idxStart > idxEnd - (pokemons.length - 1) ? idxStart - (idxEnd - (pokemons.length - 1)) : 0;
    idxEnd = pokemons.length - 1;
  }

  for (let idx = idxStart; idx <= idxEnd; idx++) {
    const pokemon = pokemons[idx];

    const key = pokemon.id || pokemon.name;
    const click = onClick || (() => null);
    const img = pokemon.sprites && <img src={pokemon.sprites.front_default} alt={pokemon.name}/>;
    const id = pokemon.id ? pokemon.id + " - " : "";

    let types;
    if (pokemon.types){
      types = (pokemon.types.reduce((prev, next) => prev + ', ' + next.type.name, '')).substring(2);
      types = <p>Type(s): {types}</p>;
    }

    const className = "pokemon-card-section" + (pokemon.selected ? " selected" : "");
    const height = pokemon.height && <p>Height: {pokemon.height}</p>;
    const weight = pokemon.weight && <p>Weight: {pokemon.weight}</p>;

    pokemonList.push(
      <Card key={pokemon.name} onClick={onClick(pokemon)} className={className}>
        {img}
        <div>
          <h3>{id}{pokemon.name}</h3>
          {types}
          <div>
            {height}
            {weight}
          </div>
        </div>
      </Card>
    );
  }

  if (idxStart > 0) {
    arrowUp = <img className="blink-image" src="assets/arrow-to-top.png"  alt="Existem mais pokemons na lista!"/>;
  }

  if (idxEnd < pokemons.length - 1) {
    arrowDown = <img className="blink-image" src="assets/arrow-to-bottom.png"  alt="Existem mais pokemons na lista!"/>;
  }

  return (
    <div id="pokemon-list" className="screen-content">
      <div className="list-warning-arrow">
        {arrowUp}
      </div>
      {pokemonList}
      <div className="list-warning-arrow">
        {arrowDown}
      </div>
    </div>
  );
}

export default PokemonList;
