import axios from 'axios';

const pokemonAPI = () => ({
  fetch(searchValue) {
    return axios.get(`https://pokeapi.co/api/v2/pokemon/${searchValue}`);
  },
  fetchByType(type) {
    return axios.get(`https://pokeapi.co/api/v2/type/${type}`);
  },
  fetchMove(moveName) {
    return axios.get(`https://pokeapi.co/api/v2/move/${moveName}`);
  },
});

export default pokemonAPI;
