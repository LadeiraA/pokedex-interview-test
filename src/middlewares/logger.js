export default store => next => action => {
  if (process.env.NODE_ENV !== 'production') {
    console.group(action.type);
    console.log('dispatching', action);
  }

  const result = next(action);

  if (process.env.NODE_ENV !== 'production') {
    console.log('next state', store.getState());
    console.groupEnd(action.type);
  }

  return result;
};
