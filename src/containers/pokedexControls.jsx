import React from 'react';
import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';

import * as pokemonActions from './../actions/pokemonActions';

class PokedexControls extends React.Component {

  constructor(props){
    super(props)

    this.toggleSearch = this.toggleSearch.bind(this);
  }

  toggleSearch() {
    const { toggleSearch, pathname } = this.props;

    return () => toggleSearch(pathname);
  }

  registerPokemon() {
    const { registerPokemon, pokemon } = this.props;
    if (pokemon) {
      return () => registerPokemon(pokemon);
    }

    return;
  }

  unregisterPokemon() {
    const { unregisterPokemon, pokemon } = this.props;
    if (pokemon) {
      return () => unregisterPokemon(pokemon.id);
    }

    return;
  }

  render() {
    const { enableControl, toBack, toPokemonDetailScreen, selectNextPokemon, selectPrevPokemon } = this.props;

    return (
      <div id="pokedex-controls">
        <div id="direction-buttons">
          <div className="row-1">
            <button
              className="button-up"
              disabled={!enableControl.direction.up}
              onClick={selectPrevPokemon}>
              <img src="assets/arrow-up.png" alt=""/>
            </button>
          </div>
          <div className="row-2">
            <button
              className="button-left"
              disabled={!enableControl.direction.left}
              onClick={toBack}>
              <img src="assets/arrow-left.png" alt=""/>
            </button>
            <button
              className="button-right"
              disabled={!enableControl.direction.right}
              onClick={toPokemonDetailScreen}>
              <img src="assets/arrow-right.png" alt=""/>
            </button>
          </div>
          <div className="row-3">
            <button
              className="button-down"
              disabled={!enableControl.direction.down}
              onClick={selectNextPokemon}>
              <img src="assets/arrow-down.png" alt=""/>
            </button>
          </div>
        </div>
        <div id="utility-buttons">
          <div className="row-1">
            <button
              className="button-search"
              disabled={!enableControl.search}
              onClick={this.toggleSearch()}>
              <img src="assets/search.png" alt=""/>
              </button>
          </div>
          <div className="row-2">
            <button
              className="button-trash"
              disabled={!enableControl.trash}
              onClick={this.unregisterPokemon()}>
              <img src="assets/trash.png" alt=""/>
            </button>
            <button
              className="button-detail"
              disabled={!enableControl.detail}
              onClick={toPokemonDetailScreen}>
              <img src="assets/detail.png" alt=""/>
            </button>
          </div>
          <div className="row-3">
            <button
              className="button-plus"
              disabled={!enableControl.plus}
              onClick={this.registerPokemon()}>
              <img src="assets/plus.png" alt=""/>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const search = state.pokemon.search
  const finded = search.finded;
  const registered = state.pokemon.registered;
  const pathname = state.router.location.pathname;

  let selected = {};
  let selectedId = 0;
  let enableControl = { direction: {} };

  if (pathname === "/") {
    selected = registered.filter(p => p.selected);
    selectedId = selected.length ? selected[0].id : 0;

    enableControl = {
      direction: {
        up: registered.length > 1 && selectedId && registered[0].id !== selectedId,
        down: registered.length > 1 && registered[registered.length - 1].id !== selectedId,
        right: selectedId,
      },
      search: true,
      trash: selectedId && registered.filter(p => p.id === selectedId).length,
      plus: selectedId && !(registered.filter(p => p.id === selectedId).length),
      detail: selectedId,
    }
  }else
  if (pathname === "/search") {
    selected = finded.filter(p => p.selected);
    selectedId = selected.length ? selected[0].id : 0;

    enableControl = {
      direction: {
        up: finded.length > 1 && selectedId && finded[0].id !== selectedId,
        down: finded.length > 1 && finded[finded.length - 1].id !== selectedId,
        right: selectedId,
      },
      search: true,
      trash: selectedId && registered.filter(p => p.id === selectedId).length,
      plus: selectedId && !(registered.filter(p => p.id === selectedId).length),
      detail: selectedId,
    }
  }else
  if (pathname === "/detail") {
    selected = finded.filter(p => p.selected);
    selectedId = selected.length ? selected[0].id : 0;

    if (!selectedId) {
      selected = registered.filter(p => p.selected);
      selectedId = selected.length ? selected[0].id : 0;
    }

    enableControl = {
      direction: {
        left: true,
      },
      trash: selectedId && registered.filter(p => p.id === selectedId).length,
      plus: selectedId && !(registered.filter(p => p.id === selectedId).length),
    }
  }else{
    enableControl = {
      direction: {
        left: true,
      },
    }
  }

  selected = selected.length ? selected[0] : null;

  return {
    pokemon: selected,
    pathname,
    enableControl,
  };
};

const mapActionsToProps = dispatch => ({
  toggleSearch(pathname){
    if (pathname !== "/search") {
      dispatch(push("/search"));
    }else{
      dispatch(push("/"));
    }
  },
  registerPokemon(pokemon){
    dispatch(push("/"));
    dispatch(pokemonActions.registerPokemon(pokemon));
  },
  unregisterPokemon(selectedId){
    dispatch(push("/"));
    dispatch(pokemonActions.unregisterPokemon(selectedId));
  },
  toPokemonDetailScreen(){
    dispatch(push("/detail"));
  },
  toBack(){
    dispatch(goBack());
  },
  selectNextPokemon(){
    dispatch(pokemonActions.selectNextPokemon());
  },
  selectPrevPokemon(){
    dispatch(pokemonActions.selectPrevPokemon());
  }
});

export default connect(mapStateToProps, mapActionsToProps)(PokedexControls);
