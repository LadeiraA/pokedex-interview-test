import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import PokemonList from './../components/pokemonList';

import * as pokemonActions from './../actions/pokemonActions';

import pokemonAPI from './../services/pokemonAPI';

class PokemonSearch extends React.Component {

  constructor(props) {
    super(props);
    this.onSearchClick = this.onSearchClick.bind(this);
    this.onSearchKeyDown = this.onSearchKeyDown.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onPokemonClick = this.onPokemonClick.bind(this);
  }

  componentDidMount() {
    if (this.searchInput) {
      this.searchInput.value = "";
      this.searchInput.focus();
    }
  }

  onSearchClick(){
    const { search, searchPokemons } = this.props;

    if (!search.searching) {
      searchPokemons(search.value);
    }
  }

  onSearchKeyDown(e){
    if (e.keyCode === 13) {
      this.onSearchClick();
    }
  }

  onSearchChange(e){
    const { updateSearchValue } = this.props;

    updateSearchValue(this.searchInput.value);
  }

  onPokemonClick(pokemon){
    const { selectPokemon } = this.props;

    return (e) => selectPokemon(pokemon.id);
  }

  render(){
    const { pokemons, search } = this.props;

    //

    const loading = search.searching && <img className="search-loading" src="assets/search-loading.gif" alt="Pesquisando, por favor aguarde..."/>;
    const message = search.done && "Não encontramos nenhum Pokemon! =(";

    //

    return (
      <div id="pokemon-search" className="screen-content">
        <h1>Buscar por Pokemons:</h1>
        <div className="search-area">
          <input
            type="text"
            ref={(input) => { this.searchInput = input; }}
            placeholder="ID ou Nome do Pokemon"
            onChange={this.onSearchChange}
            onKeyDown={this.onSearchKeyDown}
            value={search.value} />
          <button onClick={this.onSearchClick}>
            <img src="assets/search-2.png" alt="Pesquisar"/>
          </button>
        </div>
        <PokemonList pokemons={pokemons} onClick={this.onPokemonClick} emptyMessage={message} />
        {loading}
      </div>
    );
  }

}

const mapStateToProps = state => {
  return {
    pokemons: state.pokemon.search.finded || [],
    search: state.pokemon.search,
  };
};

const mapActionsToProps = dispatch => ({
  updateSearchValue(searchValue){
    dispatch(pokemonActions.updateSearchValue(searchValue));
  },
  searchPokemons(searchValue){
    dispatch(pokemonActions.searchPokemons(pokemonAPI(), searchValue));
  },
  selectPokemon(selectedId){
    dispatch(pokemonActions.selectPokemon(selectedId));
    dispatch(push("/detail"));
  }
});

export default connect(mapStateToProps, mapActionsToProps)(PokemonSearch);
