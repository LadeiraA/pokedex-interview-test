import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import Card from './../components/card';

import * as pokemonActions from './../actions/pokemonActions';

import pokemonAPI from './../services/pokemonAPI';

class PokemoTypeList extends React.Component {

  constructor(props){
    super(props);
    this.onPokemonClick = this.onPokemonClick.bind(this);
  }

  componentDidMount(){
    const { searchPokemonsByType, match: { params: { type } } } = this.props;

    searchPokemonsByType(type);
  }

  onPokemonClick(pokemon){
    const { selectPokemon } = this.props;

    return () => selectPokemon(pokemon.id);
  }

  render(){
    const { pokemons, search, match: { params: { type } } } = this.props;

    const loading = search.searching && <img className="search-loading" src="./../assets/search-loading.gif" alt="Pesquisando, por favor aguarde..."/>;
    const message = search.done && !pokemons.length && <Card>Não encontramos nenhum pokemon! =(</Card>;

    let pokemonList;
    let arrowTop;
    let arrowBottom;

    if (pokemons.length) {
      pokemonList = pokemons.map((pokemon) => {
        return (
          <Card key={pokemon.name}>
            <h3>{pokemon.name}</h3>
          </Card>
        );
      });
      pokemonList = (
        <div className="list">
          {pokemonList}
        </div>
      );
      arrowTop = <img src="./../assets/arrow-to-top.png" alt=""/>;
      arrowBottom = <img src="./../assets/arrow-to-bottom.png" alt=""/>;
    }

    return (
      <div id="pokemon-type-list" className="screen-content">
        <h1>Pokemons do Tipo {type}:</h1>
        {arrowTop}
        {pokemonList}
        {arrowBottom}
        {loading}
        {message}
      </div>
    );
  }

}

const mapStateToProps = state => {
  return {
    search: state.pokemon.searchByType,
    pokemons: state.pokemon.searchByType.finded || [],
  };
};

const mapActionsToProps = dispatch => ({
  searchPokemonsByType(type) {
    dispatch(pokemonActions.searchPokemonsByType(pokemonAPI(), type));
  },
  selectPokemon(selectedId){
    dispatch(pokemonActions.selectPokemon(selectedId));
    dispatch(push("/detail"));
  },
});

export default connect(mapStateToProps, mapActionsToProps)(PokemoTypeList);
