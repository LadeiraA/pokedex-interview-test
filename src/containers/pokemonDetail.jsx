import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Card from './../components/card';

import * as pokemonActions from './../actions/pokemonActions';

import pokemonAPI from './../services/pokemonAPI';

class PokemonDetail extends React.Component {

  onMoveClick(move){
    const { searchPokemonMove } = this.props;

    return () => searchPokemonMove(move.name);
  }

  render() {
    const { pokemon, search, searchPokemonMoveClose } = this.props;

    if (!pokemon)
      return null;

    const types = pokemon.types.reduce((prev, type) => {
      const path = `/type-list/${type.type.name}`;
      const comma = prev.length ? ', ' : '';
      prev.push(<span key={type.type.name}>{comma}<Link to={path}>{type.type.name}</Link></span>);
      return prev;
    }, []);

    const stats = pokemon.stats.map(stat => {
      return <p key={stat.stat.name}>{stat.stat.name}: {stat.base_stat}</p>
    });

    let moveDetail;
    let moves;

    if (search.visible) {
      const loading = search.searching && <img className="search-loading" src="assets/search-loading.gif" alt="Pesquisando, por favor aguarde..."/>;
      const message = search.done && !search.finded && "Não encontramos nenhuma informação! =(";

      moveDetail = (
        <Card className="moves-detail">
          <div>
            <p>{search.value}</p>
            <img src="assets/close.png" className="close-move-detail" onClick={searchPokemonMoveClose} alt=""/>
          </div>
          <div>
            {loading}
            {message}
            {search.finded}
          </div>
        </Card>
      );
    } else {
      moves = pokemon.moves.map(move => {
        return (
          <div key={move.move.name} onClick={this.onMoveClick(move.move)}>
            {move.move.name}
          </div>
        );
      });

      moves = (
        <div className="moves-list">
          <img src="assets/arrow-to-top.png" alt=""/>
          <div className="moves-list-out">
            <div className="moves-list-in">
              {moves}
            </div>
          </div>
          <img src="assets/arrow-to-bottom.png" alt=""/>
        </div>);
    }



    return (
      <div id="pokemon-detail" className="screen-content">
        <div className="main-info">
          <img src={pokemon.sprites.front_default} alt={pokemon.name}/>
          <h3>{pokemon.id} - {pokemon.name}</h3>
          <p>Type(s): {types}</p>
          <p>Height: {pokemon.height}</p>
          <p>Weight: {pokemon.weight}</p>
          {stats}
        </div>
        <div className="pokemon-moves">
          <p>Moves:</p>
          {moveDetail}
          {moves}
        </div>
      </div>
    );
  }

}

const mapStateToProps = state => {
  const search = state.pokemon.search
  const finded = search.finded;
  const registered = state.pokemon.registered;

  let selected = finded.filter(p => p.selected);

  selected = selected.length ? selected[0] : null;

  if (!selected) {
    selected = registered.filter(p => p.selected);

    selected = selected.length ? selected[0] : null;
  }

  return {
    pokemon: selected,
    search: state.pokemon.searchMove,
  };
};

const mapActionsToProps = dispatch => ({
  searchPokemonMove(moveName){
    dispatch(pokemonActions.searchPokemonMove(pokemonAPI(), moveName));
  },
  searchPokemonMoveClose(){
    dispatch(pokemonActions.searchPokemonMoveClose());
  }
});

export default connect(mapStateToProps, mapActionsToProps)(PokemonDetail);
