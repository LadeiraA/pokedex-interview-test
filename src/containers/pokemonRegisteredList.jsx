import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import PokemonList from './../components/pokemonList';

import * as pokemonActions from './../actions/pokemonActions';

class PokemonRegisteredList extends React.Component {

  constructor(props) {
    super(props);
    this.onPokemonClick = this.onPokemonClick.bind(this);
  }

  onPokemonClick(pokemon){
    const { selectPokemon } = this.props;

    return () => selectPokemon(pokemon.id);
  }

  render(){
    const { pokemons } = this.props;

    return (
      <div id="pokemon-registered" className="screen-content">
        <h1>Pokemons catalogados:</h1>
        <PokemonList pokemons={pokemons} onClick={this.onPokemonClick} emptyMessage="Você ainda não registrou nenhum pokemon! =(" />
      </div>
    );
  }

}

const mapStateToProps = state => {
  return {
    pokemons: state.pokemon.registered || [],
  };
};

const mapActionsToProps = dispatch => ({
  selectPokemon(selectedId){
    dispatch(pokemonActions.selectPokemon(selectedId));
    dispatch(push("/detail"));
  },
});

export default connect(mapStateToProps, mapActionsToProps)(PokemonRegisteredList);
