export const UPDATE_SEARCH_VALUE = 'UPDATE_SEARCH_VALUE';
export const UPDATE_SEARCH_POKEMONS = 'UPDATE_SEARCH_POKEMONS';
export const UPDATE_SEARCH_RESULT = 'UPDATE_SEARCH_RESULT';
export const UPDATE_SEARCH_FAIL = 'UPDATE_SEARCH_FAIL';
export const UPDATE_SEARCH_POKEMONS_BY_TYPE = 'SEARCH_POKEMONS_BY_TYPE';
export const UPDATE_SEARCH_RESULT_BY_TYPE = 'SEARCH_RESULT_BY_TYPE';
export const UPDATE_SEARCH_FAIL_BY_TYPE = 'SEARCH_FAIL_BY_TYPE';
export const UPDATE_SEARCH_POKEMONS_MOVE = 'SEARCH_POKEMONS_MOVE';
export const UPDATE_SEARCH_RESULT_MOVE = 'SEARCH_RESULT_MOVE';
export const UPDATE_SEARCH_FAIL_MOVE = 'SEARCH_FAIL_MOVE';
export const SEARCH_POKEMONS_MOVE_CLOSE = 'SEARCH_POKEMONS_MOVE_CLOSE';
export const SELECT_POKEMON = 'SELECT_POKEMON';
export const REGISTER_POKEMON = 'REGISTER_POKEMON';
export const UNREGISTER_POKEMON = 'UNREGISTER_POKEMON';
export const SELECT_NEXT_POKEMON = 'SELECT_NEXT_POKEMON';
export const SELECT_PREV_POKEMON = 'SELECT_PREV_POKEMON';

export const updateSearchValue = searchValue => ({
  type: UPDATE_SEARCH_VALUE,
  payload: searchValue,
});

export const updateSearchPokemons = () => ({
  type: UPDATE_SEARCH_POKEMONS,
});

export const updateSearchResult = payload => ({
  type: UPDATE_SEARCH_RESULT,
  payload,
})

export const updateSearchFail = () => ({
  type: UPDATE_SEARCH_FAIL,
});

export const searchPokemons = (pokemonAPI, searchValue) => dispatch => {
  dispatch(updateSearchPokemons());
  return pokemonAPI.fetch(searchValue)
    .then(response => {
      dispatch(updateSearchResult(response.data));
    })
    .catch(payload => {
      dispatch(updateSearchFail());
    });
}

export const updateSearchPokemonsByType = () => ({
  type: UPDATE_SEARCH_POKEMONS_BY_TYPE,
});

export const updateSearchResultByType = payload => ({
  type: UPDATE_SEARCH_RESULT_BY_TYPE,
  payload,
})

export const updateSearchFailByType = () => ({
  type: UPDATE_SEARCH_FAIL_BY_TYPE,
});

export const searchPokemonsByType = (pokemonAPI, type) => dispatch => {
  dispatch(updateSearchPokemonsByType());
  return pokemonAPI.fetchByType(type)
    .then(response => {
      dispatch(updateSearchResultByType(response.data));
    })
    .catch(payload => {
      dispatch(updateSearchFailByType());
    });
}

export const updateSearchPokemonsMove = payload => ({
  type: UPDATE_SEARCH_POKEMONS_MOVE,
  payload,
});

export const updateSearchResultMove = payload => ({
  type: UPDATE_SEARCH_RESULT_MOVE,
  payload,
})

export const updateSearchFailMove = () => ({
  type: UPDATE_SEARCH_FAIL_MOVE,
});

export const searchPokemonMove = (pokemonAPI, moveName) => dispatch => {
  dispatch(updateSearchPokemonsMove(moveName));
  return pokemonAPI.fetchMove(moveName)
    .then(response => {
      dispatch(updateSearchResultMove(response.data));
    })
    .catch(payload => {
      dispatch(updateSearchFailMove());
    });
}

export const searchPokemonMoveClose = () => ({
  type: SEARCH_POKEMONS_MOVE_CLOSE,
});

export const selectPokemon = selectedId => ({
  type: SELECT_POKEMON,
  payload: selectedId,
});

export const registerPokemon = pokemon => ({
  type: REGISTER_POKEMON,
  payload: pokemon,
});

export const unregisterPokemon = selectedId => ({
  type: UNREGISTER_POKEMON,
  payload: selectedId,
});

export const selectNextPokemon = () => ({
  type: SELECT_NEXT_POKEMON,
});

export const selectPrevPokemon = () => ({
  type: SELECT_PREV_POKEMON,
});
