import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { routerMiddleware } from 'react-router-redux'

import reducers from './reducers';

import logger from './middlewares/logger';

const store = (history) => createStore(
  reducers,
  applyMiddleware(thunk, routerMiddleware(history), logger),
);

export default store;
